## Automated EVPN deployment using Ansible

The master branch is configured according to this scheme: https://github.com/CumulusNetworks/cldemo-evpn/raw/master/images/evpn.png

Symmetric EVPN is WIP and will be tested on other branches. 

# Other info:
* you can find the demo environment here: https://github.com/CumulusNetworks/cldemo-vagrant
* documentation for EVPN on cumulus can be found here: https://docs.cumulusnetworks.com/display/DOCS/Ethernet+Virtual+Private+Network+-+EVPN
* here is an example for doing BGP on FRR using ansible: https://gitlab.com/gun1x/frr_ansible_ebgp

If you find the project interesting and want to help out, let me know.
